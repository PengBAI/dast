
import ssl
from http.server import HTTPServer
from threading import Thread
from typing import List

from .get_free_port import get_free_port


class ThreadedHTTPServer:

    def __init__(self, handler_class, https_certificate_path, requests: List[str]):
        self._port = get_free_port()
        self._handler_class = handler_class
        self._server = None
        self._server_thread = None
        self._https_certificate_path = https_certificate_path
        self._requests = requests

    def host(self) -> str:
        return '127.0.0.1'

    def port(self) -> int:
        return self._port

    def address(self):
        protocol = 'https' if self._https_certificate_path else 'http'
        return f'{protocol}://{self.host()}:{self.port()}'

    def handled_requests(self) -> List[str]:
        return self._requests

    def start(self):
        self._server = HTTPServer(('127.0.0.1', self._port), self._handler_class)

        if self._https_certificate_path:
            self._server.socket = ssl.wrap_socket(self._server.socket,
                                                  certfile=f'{self._https_certificate_path}.crt',
                                                  keyfile=f'{self._https_certificate_path}.key',
                                                  server_side=True)

        self._server_thread = Thread(target=self._server.serve_forever)
        self._server_thread.daemon = True
        self._server_thread.start()

    def stop(self):
        if self._server:
            self._server.shutdown()
            self._server.server_close()
            self._server = self._server_thread = None
