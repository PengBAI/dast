import json
from unittest import TestCase
from unittest.mock import mock_open, patch

from src.services import AddonJSONParser


class TestAddonJSONParser(TestCase):

    def test_should_output_the_expected_message(self):

        addon_data = [{
            'repoUrl': 'https://github.com/zaproxy/zap-extensions/',
            'sizeInBytes': '2510914',
            'author': 'ZAP Dev Team',
            'installationStatus': 'AVAILABLE',
            'changes': 'ZAP Changes',
            'version': '23.2.0',
            'url': 'https://example.com/spiderAjax-release-23.2.0.zap',
            'infoUrl': 'https://www.zaproxy.org/docs/desktop/addons/ajax-spider/',
            'name': 'Ajax Spider',
            'id': 'spiderAjax',
            'hash': 'SHA-256:e400d71bb80a6bf0854e6cc9ac5ea1a1c4ecaae341cdadc3886ca57c1d178430',
            'status': 'release',
        }]

        with patch('src.services.addon_json_parser.open', mock_open(read_data=json.dumps(addon_data))) as m:
            output = AddonJSONParser('path/to/json').parse()

        m.assert_called_once_with('path/to/json')

        self.assertTrue('rm -rf spiderAjax-* && wget https://example.com/spiderAjax-release-23.2.0.zap' in output)
        self.assertTrue('Upgrade ZAP add-on `Ajax Spider` to [23.2.0](https://example.com) (!__MR_ID__)' in output)
        self.assertTrue('ZAP Changes' in output)
