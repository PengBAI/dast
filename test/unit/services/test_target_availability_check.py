import unittest
from unittest.mock import Mock

from src.services.target_availability_check import TargetAvailabilityCheck
from test.unit.mock_config import ToConfig


class TestTargetAvailabilityCheck(unittest.TestCase):

    def test_status_code_returns_status_code_from_response(self):
        response = Mock(status_code=502)
        check = TargetAvailabilityCheck(True, ToConfig(), response=response)

        self.assertEqual(check.status_code(), 502)

    def test_status_code_returns_None_when_there_is_no_response(self):
        check = TargetAvailabilityCheck(True, ToConfig())

        self.assertIsNone(check.status_code())
