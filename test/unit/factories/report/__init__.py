# flake8: noqa
from .vulnerability import vulnerability

__all__ = ['vulnerability']
