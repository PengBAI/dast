from src.models import Target


def f_target(url='http://lollipops.site') -> Target:
    return Target(url)
