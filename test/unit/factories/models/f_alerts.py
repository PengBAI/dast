from src.models import Alerts
from .f_alert import f_alert


def f_alerts(*alrts) -> Alerts:
    # alerts() returns alerts with one alert
    if not len(alrts):
        return Alerts([f_alert()])

    # alerts(None) returns alerts with no alerts
    if len(alrts) == 1 and not alrts[0]:
        return Alerts([])

    # alerts(alert(), alert()) returns given alerts
    return Alerts(list(alrts))
