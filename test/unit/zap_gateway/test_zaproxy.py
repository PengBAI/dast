import unittest
from pathlib import Path
from tempfile import TemporaryDirectory
from unittest.mock import MagicMock, call, patch

from src.http_headers import HttpHeaders
from src.models import ScriptType, Target
from src.models.errors import SpiderProgressInvalidError
from src.zap_gateway import Settings, ZAProxy
from src.zap_gateway.zap_message_types import ZAP_MESSAGE_TYPES
from src.zap_gateway.zaproxy import ScanNotStartedException
from test.unit import factories
from test.unit import utilities
from test.unit.factories.models import f_rules, f_target
from test.unit.factories.zap_api import ascan, pscan
from test.unit.mock_config import ToConfig


class TestZAProxy(unittest.TestCase):

    def setUp(self):
        self.zap = MagicMock()
        self.http_headers_parser = MagicMock(parse=MagicMock(return_value=HttpHeaders()))
        self.alerts_parser = MagicMock()
        self.config = ToConfig(zap_max_connection_attempts=1, zap_connect_sleep_seconds=0.001)

        with patch('src.zap_gateway.zaproxy.ZAPDatabase') as mock_db:
            self.zaproxy = ZAProxy([], self.alerts_parser, self.config)

        # new session must get called to steal the `zap` instance
        self.zap_database = mock_db.return_value
        self.zaproxy.new_session(self.zap)

    def test_new_context_does_not_set_crawljax_to_use_chrome_before_DAST_2(self):
        self.zaproxy._config.dast_major_version = 1

        self.zaproxy.new_context(Target('http://target'))

        self.zap.ajaxSpider.set_option_browser_id.assert_not_called()
        self.zap.selenium.set_option_chrome_driver_path.assert_not_called()

    def test_new_context_sets_crawljax_to_use_chrome_in_DAST_2(self):
        self.zaproxy._config.dast_major_version = 2

        self.zaproxy.new_context(Target('http://target'))

        self.zap.ajaxSpider.set_option_browser_id.assert_called_once_with('chrome-headless')
        self.zap.selenium.set_option_chrome_driver_path.assert_called_once_with('/usr/bin/chromedriver')

    def test_set_global_exclude_urls(self):
        self.zaproxy.set_global_exclude_urls(r'^www.example.com$')
        self.zap.core.exclude_from_proxy.assert_called_once_with(r'^www.example.com$')

    def test_exclude_rules_excludes_rules(self):
        self.zap.pscan.scanners = [{'id': '14006'}, {'id': '14007'}]
        self.zap.ascan.scanners = MagicMock(return_value=[{'id': '123'}])

        self.zaproxy.exclude_rules(['123', '14006', '14007'], 'API-Minimal.policy')

        self.zap.pscan.disable_scanners.assert_called_once_with('14006,14007')
        self.zap.ascan.disable_scanners.assert_has_calls([call('123', 'API-Minimal.policy')])

    def test_exclude_rules_handles_empty_scanners(self):
        self.zap.pscan.scanners = []
        self.zap.ascan.scanners = MagicMock(return_value=[])

        self.zaproxy.exclude_rules(['123', '14006', '14007'], 'API-Minimal.policy')

        self.zap.pscan.disable_scanners.assert_called_once_with('')
        self.zap.ascan.disable_scanners.assert_has_calls([call('', 'API-Minimal.policy')])

    def test_exclude_all_passive_rules(self):
        self.zap.pscan.scanners = [{'id': '14006'}, {'id': '14007'}]
        self.zaproxy.exclude_all_passive_rules()
        self.zap.pscan.disable_scanners.assert_called_once_with('14006,14007')

    def test_only_include_rules_disables_all_rules_except_the_included_rules(self):
        self.zaproxy.only_include_rules(['14006', '14007'], 'API-Minimal.policy')

        self.zap.pscan.disable_all_scanners.assert_called_once()
        self.zap.ascan.disable_all_scanners.assert_called_once_with('API-Minimal.policy')
        self.zap.pscan.enable_scanners.assert_called_once_with('14006,14007')
        self.zap.ascan.enable_scanners.assert_called_once_with('14006,14007', 'API-Minimal.policy')

    def test_should_return_scanned_resources(self):
        with patch('src.zap_gateway.zaproxy.HttpMessagesQuery') as mock_query:
            mock_query.return_value.select.return_value = [
                {'request': {'method': 'GET', 'url': 'http://site/b'}},
                {'request': {'method': 'POST', 'url': 'http://site/a'}},
            ]

            scanned_resources = self.zaproxy.scanned_resources(Target('http://target'))

        mock_query.assert_called_once_with(self.zap_database, 'http://target')
        mock_query.return_value.select.assert_called_once_with('histtype', ZAP_MESSAGE_TYPES)
        self.assertEqual(2, len(scanned_resources))

    def test_alerts_should_fetch_messages(self):
        alerts = [factories.zap_api.alert(alert_id='1', messageId='10'),
                  factories.zap_api.alert(alert_id='2', messageId='20')]
        self.zap.alert.alerts.return_value = alerts

        with patch('src.zap_gateway.zaproxy.HttpMessagesQuery') as mock_query:
            mock_query.return_value.select.return_value = 'messages'

            self.zaproxy.alerts(Target('http://target'))

        mock_query.assert_called_once_with(self.zap_database, 'http://target')
        mock_query.return_value.select.assert_called_once_with('historyid', ['10', '20'])
        self.alerts_parser.parse.assert_called_with(alerts, 'messages')

    def test_spider_scans_returns_zap_spider_scans(self):
        self.zap.spider.scans = 'SPIDER SCANS >::::)'

        scans = self.zaproxy.spider_scans()

        self.assertEqual(scans, 'SPIDER SCANS >::::)')

    def test_scan_results_returns_results_for_scan_with_given_id(self):
        mock_results = MagicMock(return_value='MAGICAL RESULTS')
        self.zap.spider.full_results = mock_results

        results = self.zaproxy.scan_results(1)

        self.assertEqual(results, 'MAGICAL RESULTS')
        mock_results.assert_called_once_with(1)

    @patch('builtins.open')
    def test_write_html_report_writes_report(self, mock_open):
        self.zap.core.htmlreport = MagicMock(return_value=b'htmlreport')

        self.zaproxy.write_html_report('report.html')

        mock_open.assert_called_once_with('/zap/wrk/report.html', mode='wb')

        mock_file = mock_open.return_value.__enter__.return_value
        mock_file.write.assert_called_once_with(b'htmlreport')

    @patch('builtins.open')
    def test_write_html_report_encodes_non_binary_report(self, mock_open):
        self.zap.core.htmlreport = MagicMock(return_value='htmlreport')

        self.zaproxy.write_html_report('report.html')

        mock_open.assert_called_once_with('/zap/wrk/report.html', mode='wb')

        mock_file = mock_open.return_value.__enter__.return_value
        mock_file.write.assert_called_once_with(b'htmlreport')

    @patch('builtins.open')
    def test_write_xml_report_writes_report(self, mock_open):
        self.zap.core.xmlreport = MagicMock(return_value=b'xmlreport')

        self.zaproxy.write_xml_report('report.xml')

        mock_open.assert_called_once_with('/zap/wrk/report.xml', mode='wb')

        mock_file = mock_open.return_value.__enter__.return_value
        mock_file.write.assert_called_once_with(b'xmlreport')

    @patch('builtins.open')
    def test_write_xml_report_encodes_non_binary_report(self, mock_open):
        self.zap.core.xmlreport = MagicMock(return_value='xmlreport')

        self.zaproxy.write_xml_report('report.xml')

        mock_open.assert_called_once_with('/zap/wrk/report.xml', mode='wb')

        mock_file = mock_open.return_value.__enter__.return_value
        mock_file.write.assert_called_once_with(b'xmlreport')

    @patch('builtins.open')
    def test_write_md_report_writes_report(self, mock_open):
        self.zap.core.mdreport = MagicMock(return_value=b'mdreport')

        self.zaproxy.write_md_report('report.md')

        mock_open.assert_called_once_with('/zap/wrk/report.md', mode='wb')

        mock_file = mock_open.return_value.__enter__.return_value
        mock_file.write.assert_called_once_with(b'mdreport')

    @patch('builtins.open')
    def test_write_md_report_encodes_non_binary_report(self, mock_open):
        self.zap.core.mdreport = MagicMock(return_value='mdreport')

        self.zaproxy.write_md_report('report.md')

        mock_open.assert_called_once_with('/zap/wrk/report.md', mode='wb')

        mock_file = mock_open.return_value.__enter__.return_value
        mock_file.write.assert_called_once_with(b'mdreport')

    def test_executed_rules_only_includes_passive_rules_for_passive_scan(self):
        self.zap.pscan.scanners = pscan.scanners(pscan.scanner(plugin_id='1'))
        self.config.full_scan = False

        rules = f_rules()

        with patch('src.zap_gateway.zaproxy.RulesParser') as rules_parser_constructor:
            rules_parser_constructor.return_value.parse.return_value = rules
            returned_rules = self.zaproxy.executed_rules('Default Policy')

        self.zap.ascan.scanners.assert_not_called()
        self.assertEqual(rules, returned_rules)

    def test_executed_rules_includes_active_scan_rules(self):
        self.zap.pscan.scanners = pscan.scanners(pscan.scanner(plugin_id='1'))
        self.zap.ascan.scanners = MagicMock(return_value=ascan.scanners(ascan.scanner(plugin_id='2')))
        self.config.full_scan = True

        with patch('src.zap_gateway.zaproxy.RulesParser') as rules_parser:
            rules_parser.parse.return_value = f_rules()
            self.zaproxy.executed_rules('my-policy')

        self.zap.ascan.scanners.assert_called_once_with('my-policy')

    def test_remaining_records_to_passive_scan_returns_number_of_records(self):
        self.zap.pscan.records_to_scan = '666'

        num_records = self.zaproxy.remaining_records_to_passive_scan()

        self.assertEqual(num_records, 666)

    def test_should_load_zap_scripts(self):
        with TemporaryDirectory() as dirname:
            script_file = f'{dirname}/my-script.js'
            Path(script_file).touch()

            self.zaproxy.load_script(factories.models.script(script_type=ScriptType.HTTP_SENDER,
                                                             name='my-script',
                                                             file_path=script_file))

            self.zap.script.load.assert_called_once_with('my-script', 'httpsender', 'Oracle Nashorn', script_file)
            self.zap.script.enable.assert_called_once_with('my-script')

    def test_should_throw_an_error_when_script_file_does_not_exist(self):
        with self.assertRaises(RuntimeError) as error:
            self.zaproxy.load_script(factories.models.script(file_path='./not-a-file'))

        self.assertIn('Failed to load script as file ./not-a-file cannot be found', str(error.exception))

    def test_should_throw_an_error_when_script_language_cannot_be_detected(self):
        with TemporaryDirectory() as dirname, self.assertRaises(RuntimeError) as error:
            script_file = f'{dirname}/script.rb'
            Path(script_file).touch()

            self.zaproxy.load_script(factories.models.script(file_path=script_file))

        self.assertIn('Script language not supported', str(error.exception))
        self.assertIn(script_file, str(error.exception))

    def test_should_return_messages_with_ids(self):
        with patch('src.zap_gateway.zaproxy.HttpMessagesQuery') as mock_query:
            mock_query.return_value.select.return_value = [{'message_id': 1}]

            messages = self.zaproxy.messages_with_ids(['1', '2', '3'], Target('http://target'))

        mock_query.assert_called_once_with(self.zap_database, 'http://target')
        mock_query.return_value.select.assert_called_once_with('historyid', ['1', '2', '3'])
        self.assertEqual([{'message_id': 1}], messages)

    def test_should_return_no_messages_when_there_are_no_ids(self):
        messages = self.zaproxy.messages_with_ids([], Target('http://target'))

        self.assertEqual(0, len(self.zap_database.select_many.mock_calls))
        self.assertEqual(0, len(messages))

    def test_should_connect_to_the_zap_server(self):
        with utilities.httpserver.new().respond(status=200, content='{"version":"2.9.0"}').build() as server, \
             patch('src.zap_gateway.zaproxy.logging') as mock_logging:
            zap_client = self.zaproxy.connect(f'http://{server.host()}:{server.port()}')

            logged = ' '.join(map(lambda call: call[1][0], mock_logging.info.mock_calls))
            self.assertEqual(1, logged.count('looking for ZAP'))
            self.assertIn('connected to ZAP with version 2.9.0', logged)
            self.assertIsNotNone(zap_client)

    def test_should_raise_an_error_when_cannot_connect(self):
        unused_port = utilities.httpserver.get_free_port()
        self.config.zap_max_connection_attempts = 9

        with self.assertRaises(RuntimeError) as error, \
             patch('src.zap_gateway.zaproxy.logging') as mock_logging:
            self.zaproxy.connect(f'http://127.0.0.1:{unused_port}')

        logged = ' '.join(map(lambda call: call[1][0], mock_logging.info.mock_calls))
        self.assertEqual(9, logged.count('looking for ZAP'))
        self.assertIn('Failed to connect to ZAP after 9 attempts', str(error.exception))

    def test_import_api_spec_from_file_imports_spec(self):
        self.zaproxy.import_api_spec_from_file('api_spec.yml')

        self.zap.openapi.import_file.assert_called_once_with('api_spec.yml')

    def test_import_api_spec_from_url_imports_spec(self):
        self.zaproxy.import_api_spec_from_url('https://api.test', 'https://override.test')

        self.zap.openapi.import_url.assert_called_once_with('https://api.test', 'https://override.test')

    def test_urls_gets_urls_from_ZAP(self):
        self.zaproxy.urls()

        self.zap.core.urls.assert_called_once()

    def test_run_url_scan_passes_url_file_to_url_scan(self):
        self.zap._request = MagicMock(return_value='OK')
        self.zap.base = 'http://zap/JSON/'
        self.zaproxy.run_url_scan('/path/to/urls.txt')

        self.zap._request.assert_called_once_with(
            'http://zap/JSON/exim/action/importUrls/', {'filePath': '/path/to/urls.txt', 'apikey': ''})

    def test_run_active_scan_starts_an_active_scan_and_returns_scan_id(self):
        self.zap.ascan.scan = MagicMock(return_value='666')

        scan_id = self.zaproxy.run_active_scan('https://democracynow.org', 'Default Policy', '111')

        self.zap.ascan.scan.assert_called_once_with(
            'https://democracynow.org',
            recurse=True,
            scanpolicyname='Default Policy',
            contextid='111',
        )
        self.assertEqual(scan_id, 666)

    def test_run_active_scan_raises_error_if_scan_does_not_start(self):
        self.zap.ascan.scan = MagicMock(return_value='SCAN FAILED')

        with self.assertRaises(ScanNotStartedException) as error:
            self.zaproxy.run_active_scan('https://democracynow.org', 'Default Policy')

        self.assertIn(
            str(error.exception),
            'Failed to start active scan. Identified scan progress value is invalid: SCAN FAILED)',
        )

    def test_active_scan_percent_complete_returns_percent_scan_completed(self):
        self.zap.ascan.status = MagicMock(return_value='68')

        percent_complete = self.zaproxy.active_scan_percent_complete(666)

        self.zap.ascan.status.assert_called_once_with(666)
        self.assertEqual(percent_complete, 68)

    def test_active_scan_progress_returns_list_of_scanned_resources_and_details(self):
        progress = ['http://enlacezapatista.ezln.org.mx/', {'Plugin': 'Source Code Disclosure'}]
        self.zap.ascan.scan_progress = MagicMock(return_value=progress)

        details = self.zaproxy.active_scan_progress(666)

        self.zap.ascan.scan_progress.assert_called_once_with(666)
        self.assertEqual(details, progress)

    def test_run_spider_runs_zap_spider(self):
        target = f_target('http://test.target')
        self.zap.spider.scan.return_value = 'SPIDERID'

        with patch('src.zap_gateway.zaproxy.logging') as mock_logging:
            returned_id = self.zaproxy.run_spider(target)

        mock_logging.debug.assert_called_once_with('Spider context: Target Context')
        self.zap.spider.scan.assert_called_once_with(target, contextname=Settings.CONTEXT_NAME)
        self.assertEqual(returned_id, 'SPIDERID')

    def test_spider_progress_percentage_returns_spider_status(self):
        spider_status = '50'
        self.zap.spider.status.return_value = spider_status

        returned_status = self.zaproxy.spider_progress_percentage('SPIDERID')

        self.zap.spider.status.assert_called_once_with('SPIDERID')
        self.assertEqual(returned_status, 50)

    def test_spider_progress_percentage_raises_error_if_invalid(self):
        spider_status = 'error'
        self.zap.spider.status.return_value = spider_status

        with self.assertRaises(SpiderProgressInvalidError) as error:
            self.zaproxy.spider_progress_percentage('SPIDERID')

        self.assertEqual(
            'Failed to spider progress. Identified spider progress percentage is invalid: error',
            str(error.exception),
        )

    def test_set_ajax_spider_maximum_run_time_sets_length(self):
        self.zaproxy.set_ajax_spider_maximum_run_time('666')

        self.zap.ajaxSpider.set_option_max_duration.assert_called_once_with('666')

    def test_run_ajax_spider_starts_spider_with_given_target(self):
        target = f_target('https://fake.test')

        self.zaproxy.run_ajax_spider(target)

        self.zap.ajaxSpider.scan.assert_called_once_with(target)

    def test_ajax_spider_status_returns_spider_status(self):
        self.zap.ajaxSpider.status = 'running'

        status = self.zaproxy.ajax_spider_status()

        self.assertEqual(status, 'running')

    def test_ajax_spider_results_count_returns_num_results(self):
        self.zap.ajaxSpider.number_of_results = 420

        results_count = self.zaproxy.ajax_spider_results_count()

        self.assertEqual(results_count, 420)

    def test_update_addons(self):
        self.zap.autoupdate.updated_addons = 'addons to update as JSON'

        self.assertEqual(self.zaproxy.updated_addons(), 'addons to update as JSON')

    def test_sets_cookies_for_httpsessions(self):
        cookies = [
            {'name': 'name1', 'value': 'value1', 'domain': '', 'path': '', 'expires': 0,
             'size': 0, 'httpOnly': True, 'secure': True, 'session': True, 'priority': '',
             'time_observed': '2021-03-03T09:12:15.886752+09:00'},
            {'name': 'name2', 'value': 'value2', 'domain': '', 'path': '', 'expires': 0,
             'size': 0, 'httpOnly': True, 'secure': True, 'session': True, 'priority': '',
             'time_observed': '2021-03-03T09:12:15.886753+09:00'},
        ]

        self.zaproxy.create_zap_httpsession(cookies, Target('http://target'))

        self.zap.httpsessions.add_session_token.assert_called_once_with('http://target', 'dast_session')
        self.zap.httpsessions.create_empty_session.assert_called_once_with('http://target', 'auth-session')
        self.zap.httpsessions.set_active_session.assert_called_once_with('http://target', 'auth-session')

        self.zap.httpsessions.set_session_token_value.assert_has_calls(
            [
                call('http://target', 'auth-session', 'name1', 'value1'),
                call('http://target', 'auth-session', 'name2', 'value2'),
            ])
