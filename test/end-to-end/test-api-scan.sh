#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create test >/dev/null
  docker run --name api-server \
    -v "${PWD}/fixtures/rest-api":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/rest-api/nginx.conf":/etc/nginx/conf.d/default.conf \
    --network test \
    -d nginx:1.22.0-alpine >/dev/null

  true
}

teardown_suite() {
  docker rm --force api-server  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_full_api_scan() {
  docker run --rm \
    -v "${PWD}/fixtures/rest-api":/zap/wrk \
    --network test \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_EXCLUDE_RULES=100000,100001 \
    --env DAST_REQUEST_HEADERS='Authorization: Bearer secrety.secret' \
    "${BUILT_IMAGE}" \
    /analyze -d --api-specification open-api-v2.yml \
    >output/test_full_api_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < "${PWD}/fixtures/rest-api/gl-dast-report.json" > output/report_test_full_api_scan.json

  diff -u <(./normalize_dast_report.py expect/test_full_api_scan.json) \
          <(./normalize_dast_report.py output/report_test_full_api_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  grep -q 'Active Scan complete' output/test_full_api_scan.log
  assert_equals "0" "$?" "A full scan was not performed"

  ./verify-dast-schema.py output/report_test_full_api_scan.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}

test_full_api_scan_with_only_include_rules() {
  # Run a full scan, only include a rule ID that does not exist
  # Therefore, no rules should execute
  docker run --rm \
    -v "${PWD}/fixtures/rest-api":/zap/wrk \
    --network test \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_ONLY_INCLUDE_RULES=666666666 \
    --env DAST_REQUEST_HEADERS='Authorization: Bearer secrety.secret' \
    "${BUILT_IMAGE}" \
    /analyze -d --api-specification open-api-v2.yml \
    >output/test_full_api_scan_with_only_include_rules.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  grep -q 'Active scan rule' output/test_full_api_scan_with_only_include_rules.log
  assert_equals "1" "$?" "Expected no active scan rules to run"

  jq . < "${PWD}/fixtures/rest-api/gl-dast-report.json" > output/report_test_full_api_scan_with_only_include_rules.json

  grep -q 'vulnerabilities": \[\]' output/report_test_full_api_scan_with_only_include_rules.json
  assert_equals "0" "$?" "Expected no vulnerabilities to be found"
}

test_baseline_api_scan() {
  docker run --rm \
    -v "${PWD}/fixtures/rest-api":/zap/wrk \
    --network test \
    --env DAST_EXCLUDE_RULES=100000 \
    --env DAST_REQUEST_HEADERS='Authorization: Bearer secrety.secret' \
    "${BUILT_IMAGE}" \
    /analyze -d --api-specification http://api-server/open-api-v2.json -O api-server \
    >output/test_baseline_api_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < "${PWD}/fixtures/rest-api/gl-dast-report.json" > output/report_test_baseline_api_scan.json

  diff -u <(./normalize_dast_report.py expect/test_baseline_api_scan.json) \
          <(./normalize_dast_report.py output/report_test_baseline_api_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  grep -q 'Active Scan progress' output/test_baseline_api_scan.log
  assert_equals "1" "$?" "A baseline scan was not performed"

  ./verify-dast-schema.py output/report_test_baseline_api_scan.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}

test_openapiv3_api_scan() {
  docker run --rm \
    -v "${PWD}/fixtures/rest-api":/zap/wrk \
    --network test \
    --env DAST_EXCLUDE_RULES=100000,100001 \
    --env DAST_REQUEST_HEADERS='Authorization: Bearer secrety.secret' \
    --env DAST_API_OPENAPI=http://api-server/open-api-v3.yml \
    --env DAST_API_HOST_OVERRIDE=api-server:80 \
    --env DAST_AUTO_UPDATE_ADDONS=false \
    "${BUILT_IMAGE}" \
    /analyze -d \
    >output/test_openapiv3_api_scan.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < "${PWD}/fixtures/rest-api/gl-dast-report.json" > output/report_test_openapiv3_api_scan.json

  diff -u <(./normalize_dast_report.py expect/test_openapiv3_api_scan.json) \
          <(./normalize_dast_report.py output/report_test_openapiv3_api_scan.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_openapiv3_api_scan.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}
