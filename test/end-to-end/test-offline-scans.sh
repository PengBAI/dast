#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create --internal no-internet >/dev/null

  docker run \
    --name basic-site-no-internet \
    -v "${PWD}/fixtures/basic-site":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/basic-site/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key \
    --network no-internet -d nginx:1.22.0 >/dev/null

  true
}

teardown_suite() {
  docker rm --force basic-site-no-internet  >/dev/null 2>&1
  docker network rm no-internet >/dev/null 2>&1
  true
}

test_offline_scans() {
  docker run --rm \
    -v "${PWD}":/output \
    --network container:basic-site-no-internet \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://localhost -z"-silent" \
    >output/test_offline_scans.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_offline_scans.json

  diff -u <(./normalize_dast_report.py expect/test_offline_scans.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  grep -q 'java.net.UnknownHostException' output/test_offline_scans.log
  assert_equals "1" "$?" "Scan attempts to access internet"

  ./verify-dast-schema.py output/report_test_offline_scans.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}
