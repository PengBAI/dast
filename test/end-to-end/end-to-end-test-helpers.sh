#!/usr/bin/env bash

set -e

setup_test_dependencies() {
  mkdir -p output

  # install jq if not present
  command -v jq >/dev/null || apk add jq
}

run_basic_site() {
  docker network create test >/dev/null

  docker run \
    --name nginx \
    -v "${PWD}/fixtures/basic-site":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/basic-site/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key \
    --network test -d nginx:1.22.0 >/dev/null
}

run_ajax_spider_site() {
    docker network create test >/dev/null

    docker run --rm \
    -v "${PWD}/fixtures/ajax-spider":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/ajax-spider/nginx.conf":/etc/nginx/conf.d/default.conf \
    --name nginx \
    --network test \
    -d nginx:1.22.0 >/dev/null
  true
}

run_echo_site() {
  docker network create test >/dev/null

  docker run --rm \
    --name nginx \
    --network test \
    -v "${PWD}/fixtures/echo":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/echo/nginx.conf":/etc/nginx/conf.d/default.conf \
    -d openresty/openresty:1.19.9.1-9-buster-fat >/dev/null

  true
}

run_multi_page_site() {
  docker network create test >/dev/null

  docker run --rm \
    --name nginx \
    -v "${PWD}/fixtures/basic-multi-page-site":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/basic-multi-page-site/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.crt":/etc/nginx/self-signed.crt \
    -v "${PWD}/../unit/fixtures/certificates/self-signed.key":/etc/nginx/self-signed.key \
    --network test -d nginx:1.22.0 >/dev/null
}

run_mutual_tls_site() {
  docker network create test >/dev/null

  docker run --rm \
    --name nginx \
    -v "${PWD}/fixtures/mutual-tls/src":/usr/share/nginx/html:ro \
    -v "${PWD}/fixtures/mutual-tls/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/fixtures/mutual-tls/certs":/etc/nginx/certs \
    --network test -d nginx:1.22.0 >/dev/null
}
