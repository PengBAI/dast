#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_multi_page_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_dont_exclude_urls() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_browserker_dont_exclude_urls.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_browserker_dont_exclude_urls.json

  diff -u <(./normalize_dast_report.py expect/test_browserker_dont_exclude_urls.json) \
          <(./normalize_dast_report.py output/report_test_browserker_dont_exclude_urls.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}

test_browserker_exclude_urls() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_EXCLUDE_URLS="http://nginx/page3.html,http://nginx/page4.html" \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx >output/test_browserker_exclude_urls.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_browserker_exclude_urls.json

  diff -u <(./normalize_dast_report.py expect/test_browserker_exclude_urls.json) \
          <(./normalize_dast_report.py output/report_test_browserker_exclude_urls.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"
}
