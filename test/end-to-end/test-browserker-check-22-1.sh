#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  docker network create test >/dev/null

  docker run \
    --name check-22-1 \
    -v "${PWD}/fixtures/check-22-1/nginx.conf":/etc/nginx/conf.d/default.conf \
    -v "${PWD}/fixtures/check-22-1/index.html:/usr/share/nginx/html/index.html" \
    --network test -d openresty/openresty:1.19.9.1-9-buster-fat >/dev/null 2>&1
}

teardown_suite() {
  docker rm --force check-22-1  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_active_check_22_1() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_FF_ENABLE_BROWSER_BASED_ATTACKS="true" \
    --env DAST_BROWSER_LOG="brows:debug,chrom:trace" \
    --env DAST_BROWSER_DEVTOOLS_LOG="Default:suppress; Fetch:messageAndBody,truncate:2000; Network:messageAndBody,truncate:2000" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://check-22-1 >output/test_browserker_active_check_22_1.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . <gl-dast-report.json >output/report_test_browserker_active_check_22_1.json
  rm -rf ./browserker_data
  mv ./findings.json output/report_test_browserker_active_check_22_1_findings.json
  mv ./gl-dast-crawl-graph.svg output/report_test_browserker_active_check_22_1-crawl-graph.svg

  diff -u <(./normalize_dast_report.py expect/test_browserker_active_check_22_1.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_browserker_active_check_22_1.json
}
