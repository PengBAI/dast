#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit


# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_echo_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_custom_headers_are_only_sent_to_target_url_domain() {
  docker run --rm -v "${PWD}":/output \
    --network test \
    --env DAST_DEBUG=1 \
    --env DAST_ZAP_LOG_CONFIGURATION="rootLogger.level=debug" \
    --env DAST_REQUEST_HEADERS='Authorization: Bearer secret-token' \
    --env DAST_ZAP_CONNECT_SLEEP_SECONDS='5' \
    "${BUILT_IMAGE}" \
    /analyze -j -t http://nginx -d \
    >output/test_custom_headers_are_only_sent_to_target_url_domain.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  docker run --rm --network test -v "${PWD}":/output "${BUILT_IMAGE}" curl --silent http://nginx/recorded-requests >output/recorded-requests.txt
  assert_equals "0" "$?" "Expected recorded requests to be fetched without errors"

  grep "http://127.0.0.1/styles.css was filtered by a filter with reason: OUT_OF_CONTEXT" output/test_custom_headers_are_only_sent_to_target_url_domain.log >/dev/null
  assert_equals "0" "$?" "A resource from another domain should not load"

  grep "authorization: Bearer secret-token  (from host 127.0.0.1)" output/recorded-requests.txt >/dev/null
  assert_equals "1" "$?" "Secret token should not be exposed to the external domains"
}
