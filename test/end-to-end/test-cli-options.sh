#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create test >/dev/null
  docker run --rm --name nginx -v "${PWD}/fixtures/basic-site":/usr/share/nginx/html:ro --network test -d nginx:1.22.0 >/dev/null
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_jvm_args_can_be_set() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    -e DAST_ZAP_CLI_OPTIONS="-Xmx4072m" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://nginx >output/test_jvm_args_can_be_set.log 2>&1

  grep -q "Using JVM args: -Xmx4072m" output/test_jvm_args_can_be_set.log | \
  assert_equals "0" "$?" "JVM args not found in log file"
}

# Note that reports will always land in the /output.
test_reports_can_be_exported() {
  docker run --rm -v "${PWD}":/output --network test \
    "${BUILT_IMAGE}" /analyze \
    -d \
    -t http://nginx \
    -r report.html \
    -w report.md \
    -x report.xml \
    >output/test_reports_can_be_exported.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  cp report.md output/report_test_reports_can_be_exported.md
  cp report.html output/report_test_reports_can_be_exported.html
  cp report.xml output/report_test_reports_can_be_exported.xml

  grep -Fq '<td><a href="#10038">Content Security Policy (CSP) Header Not Set</a></td>' report.html
  assert_equals "0" "$?" "Html report differs from expectation"

  grep -Fq '| Content Security Policy (CSP) Header Not Set | Medium | 5 |' report.md
  assert_equals "0" "$?" "Markdown report differs from expectation"

  grep -Fq '<alert>Content Security Policy (CSP) Header Not Set</alert>' report.xml
  assert_equals "0" "$?" "Xml report differs from expectation"
}

test_zap_log_config_can_be_customized() {
CUSTOM_CONFIGURATION="logger.httpsender.name=org.parosproxy.paros.network.HttpSender;logger.httpsender.level=debug;logger.sitemap.name=org.parosproxy.paros.model.SiteMap;logger.sitemap.level=debug;"

  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    -e DAST_ZAP_LOG_CONFIGURATION="$CUSTOM_CONFIGURATION" \
    "${BUILT_IMAGE}" \
    /analyze -d -t http://nginx >output/test_zap_log_config_can_be_customized.log 2>&1


  grep -q "DEBUG org.parosproxy.paros.network.HttpSender" output/test_zap_log_config_can_be_customized.log | \
  grep -q "DEBUG org.parosproxy.paros.model.SiteMap" output/test_zap_log_config_can_be_customized.log
  assert_equals "0" "$?" "HttpSender DEBUG messages not found in log file"
}

test_help_documentation() {
  docker run --rm \
    "${BUILT_IMAGE}" /analyze --help \
     >output/test_help_documentation.log 2>&1
  assert_equals "0" "$?" "Help documentation was not outputed, test failed."

  grep -q "usage: analyze.py \[-h\] \[-t DAST_WEBSITE\]" output/test_help_documentation.log &&
  grep -q "auth-exclude-urls DAST_EXCLUDE_URLS" output/test_help_documentation.log
  assert_equals "0" "$?" "Error message not present"
}

test_invalid_configuration() {
  docker run --rm \
    "${BUILT_IMAGE}" /analyze \
     >output/test_invalid_configuration.log 2>&1
  assert_equals "1" "$?" "Expected DAST to fail, but it did not."

  grep -q "InvalidConfigurationError: Either DAST_WEBSITE, DAST_API_OPENAPI (deprecated), or DAST_API_SPECIFICATION must be set." output/test_invalid_configuration.log
  assert_equals "0" "$?" "Error message not present"
}
