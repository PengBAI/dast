#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies

  docker network create test >/dev/null

  docker run \
    --name nginx-memory \
    --network test \
    -v "${PWD}/fixtures/large-responses":/usr/share/nginx/html \
    -v "${PWD}/fixtures/large-responses/nginx.conf":/etc/nginx/conf.d/default.conf \
    -d nginx:1.22.0

  # generate a 1MB file
  docker exec nginx-memory dd if=/dev/urandom of=/usr/share/nginx/html/large-response.dat count=1072 bs=1024 >/dev/null 2>&1

  true
}

teardown_suite() {
  docker rm --force nginx-memory  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_can_scan_sites_with_large_sites_with_small_amounts_of_memory() {
  #  Only give ZAP a small amount of memory, there are 50 responses each having a body of 1MB
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_FULL_SCAN_ENABLED=true \
    --env DAST_ZAP_CLI_OPTIONS=-Xmx200m \
    "${BUILT_IMAGE}" /analyze -d -t http://nginx-memory \
    >output/test_can_scan_sites_with_large_sites_with_small_amounts_of_memory.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . < gl-dast-report.json > output/report_test_can_scan_sites_with_large_sites_with_small_amounts_of_memory.json

  grep -q 'Using JVM args: -Xmx200m' output/test_can_scan_sites_with_large_sites_with_small_amounts_of_memory.log
  assert_equals "0" "$?" "Logged output differs from expectation"

  grep -q 'java.lang.OutOfMemoryError: Java heap space' output/test_can_scan_sites_with_large_sites_with_small_amounts_of_memory.log
  assert_equals "1" "$?" "Scan ran out of memory"

  ./verify-dast-schema.py output/report_test_can_scan_sites_with_large_sites_with_small_amounts_of_memory.json
  assert_equals "0" "$?" "DAST report does not conform to DAST schema"
}
