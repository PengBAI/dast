#!/bin/bash
# Testing framework: https://github.com/pgrange/bash_unit

# shellcheck disable=SC1091
source "./end-to-end-test-helpers.sh"

BUILT_IMAGE=${BUILT_IMAGE:-dast}

setup_suite() {
  setup_test_dependencies
  run_basic_site
  true
}

teardown_suite() {
  docker rm --force nginx  >/dev/null 2>&1
  docker network rm test >/dev/null 2>&1
  true
}

test_browserker_scan_bypassing_zap() {
  docker run --rm \
    -v "${PWD}":/output \
    --network test \
    --env DAST_BROWSER_SCAN=true \
    --env DAST_BROWSER_DEVTOOLS_LOG="Default:messageAndBody,truncate:2000" \
    --env DAST_BROWSER_LOG="loglevel:trace" \
    --env DAST_BROWSER_INCLUDE_ONLY_RULES="16.8" \
    --env DAST_FF_BYPASS_ZAP=true \
    "${BUILT_IMAGE}" \
    /analyze -t https://nginx >output/test_browserker_scan_bypassing_zap.log 2>&1
  assert_equals "0" "$?" "Expected to exit without errors"

  jq . <gl-dast-report.json >output/report_test_browserker_scan_bypassing_zap.json
  rm -rf ./browserker_data
  mv ./report.dot output/report_test_browserker_scan_bypassing_zap_report.dot
  mv ./findings.json output/report_test_browserker_scan_bypassing_zap_findings.json
  mv ./gl-dast-crawl-graph.svg output/report_test_browserker_scan_bypassing_zap-crawl-graph.svg

  diff -u <(./normalize_dast_report.py expect/test_browserker_scan_bypassing_zap.json) \
          <(./normalize_dast_report.py gl-dast-report.json)
  assert_equals "0" "$?" "Analyze results differ from expectations"

  ./verify-dast-schema.py output/report_test_browserker_scan_bypassing_zap.json
}
