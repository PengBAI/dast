server {
    root         /usr/share/nginx/html;
    listen       80;
    server_name  localhost;
    types { } default_type "application/json";

    # redirect from root URLs
    rewrite ^/$ /v1 redirect;
    rewrite ^/v1$ /v1/trees redirect;

    # GET/POST /v1/tree/:tree_id
    location = /v1/trees {
        if ($http_authorization != "Bearer secrety.secret") { return 401; }

        if ($request_method = GET)
        {
            set $BODY '{"payment": "Please pay using your American Express Credit Card with number 3782 8224 6310005."';
            set $BODY '$BODY, "trees": [';
            set $BODY '$BODY {"tree": { "ref": "/v1/trees/1" }},';
            set $BODY '$BODY {"tree": { "ref": "/v1/trees/2" }},';
            set $BODY '$BODY {"tree": { "ref": "/v1/trees/3" }}';
            set $BODY '$BODY ]}';

            return 200 $BODY;
        }

        if ($request_method = POST)
        {
            return 201 '{"tree": { "ref": "/v1/trees/1" }}';
        }

        limit_except GET POST { deny all; }
    }

    # GET /v1/tree/:tree_id
    location = /v1/trees/1 {
        if ($http_authorization != "Bearer secrety.secret") { return 401; }

        return 200 '{"tree": {"name": "banksia" }}';
        limit_except GET { deny all; }
    }

    location = /v1/trees/2 {
        if ($http_authorization != "Bearer secrety.secret") { return 401; }

        return 200 '{"tree": {"name": "eucalypt" }}';
        limit_except GET { deny all; }
    }

    location = /v1/trees/3 {
        if ($http_authorization != "Bearer secrety.secret") { return 401; }

        return 200 '{"tree": {"name": "wattle" }}';
        limit_except GET { deny all; }
    }

    location ~ /v1/trees/* {
        if ($http_authorization != "Bearer secrety.secret") { return 401; }

        return 404;
        limit_except GET { deny all; }
    }

    # DELETE /v1/tree/:tree_id
    location ~ /v1/tree/*/delete {
        if ($http_authorization != "Bearer secrety.secret") { return 401; }

        return 403;
        limit_except DELETE { deny all; }
    }

    # Error pages
    error_page 404 /404.html;
    location /404.html {
        return 404 '{"error": {"status_code": 404,"status": "Not Found"}}';
    }
}

