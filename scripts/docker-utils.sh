#!/usr/bin/env bash

set -e

PROJECT_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)/..")"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/script-utils.sh"

# path_to_dast_image_name checks presence of and returns the path to the file containing the built DAST image
path_to_dast_image_name() {
  local image_file_name="$1"
  verify_has_value "$image_file_name" "Aborting, image file name has not been supplied to ${FUNCNAME[0]}"

  local image_file
  image_file=$(realpath "$PROJECT_DIR/$image_file_name")

  if ! [[ -f $image_file ]]; then
    error "Aborting, unable to determine previously built DAST image as file containing name doesn't exist: '$image_file'"
  fi

  echo "$image_file"
}

# dast_image returns the image name of the built DAST image
dast_image() {
  local image_file_name="$1"
  verify_has_value "$image_file_name" "Aborting, image file name has not been supplied to ${FUNCNAME[0]}"

  cat "$(path_to_dast_image_name "$image_file_name")"
}

# tag_and_push_image tags a docker image
# arguments: [from] [to]
# The from image must be available locally
# You must be logged into the registry hosting the to image
tag_and_push_image() {
  local from="$1"
  local to="$2"

  verify_has_value "$from" "Aborting, unable to tag image as the from location has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$to" "Aborting, unable to tag image as the to location has not been supplied to ${FUNCNAME[0]}"

  docker tag "$from" "$to"
  docker push "$to"
}


# push_dast_to_registry pushes the DAST Docker image to a remote registry.
# Applies multiple tags to the same image.
# arguments: [Registry Username] [Registry Password] [CI Registry] [CI Registry Image] [Major Version] [Minor Version] [Patch Version]
push_dast_to_registry() {
  local registry_username="$1"
  local registry_password="$2"
  local ci_registry="$3"
  local ci_registry_image="$4"
  local major_version="$5"
  local minor_version="$6"
  local patch_version="$7"
  local dast
  dast="$(dast_image 'built_image.txt')"

  local future_dast
  future_dast="$(dast_image 'built_future_image.txt')"

  verify_has_value "$registry_username" "Aborting, ci registry username has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$registry_password" "Aborting, ci registry password has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$ci_registry" "Aborting, ci registry has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$ci_registry_image" "Aborting, ci registry image has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$major_version" "Aborting, major version has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$minor_version" "Aborting, minor version has not been supplied to ${FUNCNAME[0]}"
  verify_has_value "$patch_version" "Aborting, patch version has not been supplied to ${FUNCNAME[0]}"

  docker login -u "$registry_username" -p "$registry_password" "$ci_registry"
  docker pull "$dast"
  docker pull "$future_dast"

  tag_and_push_image "$dast" "$ci_registry_image:$major_version.$minor_version.$patch_version"
  tag_and_push_image "$dast" "$ci_registry_image:$major_version.$minor_version"
  tag_and_push_image "$dast" "$ci_registry_image:$major_version"
  tag_and_push_image "$dast" "$ci_registry_image:latest"
  tag_and_push_image "$future_dast" "$ci_registry_image:$(("$major_version" + 1)).0.0-alpha"
}
