#!/usr/bin/env bash

set -eu

SCRIPTS_DIR="$(realpath "$(cd "$(dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd)")"
BUILT_IMAGE=${BUILT_IMAGE:-dast}

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$SCRIPTS_DIR/issue-utils.sh"

# shellcheck disable=SC1090 # (dont follow, will be checked separately)
source "$PROJECT_DIR/scripts/script-utils.sh"

zap_addons_that_require_update() {
  docker run --rm -v "${PWD}":/zap/wrk "$BUILT_IMAGE" \
    /analyze --write-addons-to-update-file true >/dev/null

  if [ -s addons.json ]; then
    echo "Updates required"
    invoke dast.parse-addon-json
    ADDONS=$(jq '.[] | (.id + " " + .version)' addons.json)
  else
    echo "No updates necessary."
  fi
}

create_zap_issue() {
  verify_has_value "$GITLAB_API_TOKEN" "Aborting, GitLab CI Token with API access has not been supplied to script."
  verify_has_value "$GITLAB_PROJECT_ID" "Aborting, GitLab Project ID has not been supplied to script."

  ISSUE=$(zap_addons_issue_exists "$GITLAB_API_TOKEN" "$GITLAB_PROJECT_ID")

  if [[ "$ISSUE" != "" ]]; then
      echo "$ISSUE"
      echo "Issue already created, no further action necessary."
  else
      create_zap_addons_issue "$GITLAB_API_TOKEN" "$GITLAB_PROJECT_ID" "$ADDONS"
  fi
}


log "Checking for updates"
zap_addons_that_require_update

# Passing in the -i flag will create an issue in GitLab for the update
while getopts ":i" opt; do
    case ${opt} in
        i )
            if [[ "$ADDONS" != "" ]]; then
                log "Create an issue to update ZAP addons"
                create_zap_issue
            fi
        ;;
        \? )
            >&2 echo "Usage: ${BASH_SOURCE[0]} [--i]"
            exit 1
            ;;
    esac
done
