ARG BASE_IMAGE=registry.gitlab.com/security-products/dast/browserker:0.0.121

FROM $BASE_IMAGE as compiled_dependencies
ARG DEBIAN_FRONTEND=noninteractive
USER root
RUN apt-get update && \
    apt-get install --assume-yes --no-install-recommends \
        gcc \
        python3-dev \
        python3-pip \
        python-is-python3 \
        && \
    apt-get clean && \
        rm -rf /var/lib/apt/lists/*

RUN pip3 install memory_profiler matplotlib

FROM $BASE_IMAGE
ARG BUILDING_FOR=now
ARG DEBIAN_FRONTEND=noninteractive
ARG CHROMEDRIVER_VERSION=105.0.5195.52
ARG ZAP_VERSION=2.12.0
ARG COMMUNITY_SCRIPTS_URL=https://raw.githubusercontent.com/zaproxy/community-scripts/f95c690c4be594db79b5ff5f27cae2ef5e2da396/
ARG ZAP_POLICIES_URL=https://raw.githubusercontent.com/zaproxy/zaproxy/efb404d38280dc9ecf8f88c9b0c658385861bdcf/docker/policies/

USER root

# Install Python and Java
# Install jq to allow users to post process the generated DAST report
RUN apt-get update && \
    apt-get install --assume-yes --no-install-recommends \
        jq \
        openjdk-11-jdk \
        wget \
        curl \
        ca-certificates \
        unzip \
        python3-pip \
        python-is-python3

# Install chromedriver version to match Chrome version installed by browserker
RUN cd /opt && wget https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip && \
    unzip chromedriver_linux64.zip && \
    rm -f chromedriver_linux64.zip && \
    ln -s /opt/chromedriver /usr/bin/chromedriver

ARG BROWSERKER_UID=1000
ARG ZAP_UID=1001

# Install ZAP
RUN usermod --uid $BROWSERKER_UID gitlab && \
    useradd --uid $ZAP_UID --create-home --shell /usr/bin/bash zap && \
    wget "https://github.com/zaproxy/zaproxy/releases/download/v${ZAP_VERSION}/ZAP_${ZAP_VERSION}_Linux.tar.gz" && \
    tar xvf "ZAP_${ZAP_VERSION}_Linux.tar.gz" && \
    mv "ZAP_${ZAP_VERSION}"/ /zap && \
    rm "ZAP_${ZAP_VERSION}_Linux.tar.gz" && \
    #The following is a work-around to prevent a Log4j vulnerability, see https://gitlab.com/gitlab-org/gitlab/-/issues/348157
    sed -i "s/exec java /exec java -Dlog4j2.formatMsgNoLookups=true /g" /zap/zap.sh && \
    cd /zap/plugin && \
    rm -rf accessControl-* && wget https://github.com/zaproxy/zap-extensions/releases/download/accessControl-v8/accessControl-alpha-8.zap && \
    rm -rf alertFilters-* && wget https://github.com/zaproxy/zap-extensions/releases/download/alertFilters-v14/alertFilters-release-14.zap && \
    rm -rf ascanrules-* && wget https://github.com/zaproxy/zap-extensions/releases/download/ascanrules-v49/ascanrules-release-49.zap && \
    rm -rf ascanrulesBeta-* && wget https://github.com/zaproxy/zap-extensions/releases/download/ascanrulesBeta-v43/ascanrulesBeta-beta-43.zap && \
    rm -rf automation-* && wget https://github.com/zaproxy/zap-extensions/releases/download/automation-v0.19.0/automation-beta-0.19.0.zap && \
    rm -rf bruteforce-* && wget https://github.com/zaproxy/zap-extensions/releases/download/bruteforce-v12/bruteforce-beta-12.zap && \
    rm -rf callhome-* && wget https://github.com/zaproxy/zap-extensions/releases/download/callhome-v0.5.0/callhome-release-0.5.0.zap && \
    rm -rf commonlib-* && wget https://github.com/zaproxy/zap-extensions/releases/download/commonlib-v1.11.0/commonlib-release-1.11.0.zap && \
    rm -rf diff-* && wget https://github.com/zaproxy/zap-extensions/releases/download/diff-v12/diff-beta-12.zap && \
    rm -rf directorylistv1-* && wget https://github.com/zaproxy/zap-extensions/releases/download/directorylistv1-v5/directorylistv1-release-5.zap && \
    rm -rf domxss-* && wget https://github.com/zaproxy/zap-extensions/releases/download/domxss-v14/domxss-release-14.zap && \
    rm -rf encoder-* && wget https://github.com/zaproxy/zap-extensions/releases/download/encoder-v0.7.0/encoder-beta-0.7.0.zap && \
    rm -rf exim-* && wget https://github.com/zaproxy/zap-extensions/releases/download/exim-v0.3.0/exim-beta-0.3.0.zap && \
    rm -rf formhandler-* && wget https://github.com/zaproxy/zap-extensions/releases/download/formhandler-v6.1.0/formhandler-beta-6.1.0.zap && \
    rm -rf fuzz-* && wget https://github.com/zaproxy/zap-extensions/releases/download/fuzz-v13.8.0/fuzz-beta-13.8.0.zap && \
    rm -rf fuzzdb-* && wget https://github.com/zaproxy/zap-extensions/releases/download/fuzzdb-v9/fuzzdb-release-9.zap && \
    rm -rf gettingStarted-* && wget https://github.com/zaproxy/zap-extensions/releases/download/gettingStarted-v14/gettingStarted-release-14.zap && \
    rm -rf graaljs-* && wget https://github.com/zaproxy/zap-extensions/releases/download/graaljs-v0.3.0/graaljs-alpha-0.3.0.zap && \
    rm -rf graphql-* && wget https://github.com/zaproxy/zap-extensions/releases/download/graphql-v0.12.0/graphql-alpha-0.12.0.zap && \
    rm -rf download/* && wget https://github.com/zaproxy/zap-core-help/releases/download/help-v15/help-release-15.zap && \
    rm -rf hud-* && wget https://github.com/zaproxy/zap-hud/releases/download/v0.15.0/hud-beta-0.15.0.zap && \
    rm -rf invoke-* && wget https://github.com/zaproxy/zap-extensions/releases/download/invoke-v12/invoke-beta-12.zap && \
    rm -rf network-* && wget https://github.com/zaproxy/zap-extensions/releases/download/network-v0.5.0/network-beta-0.5.0.zap && \
    rm -rf oast-* && wget https://github.com/zaproxy/zap-extensions/releases/download/oast-v0.13.0/oast-beta-0.13.0.zap && \
    rm -rf onlineMenu-* && wget https://github.com/zaproxy/zap-extensions/releases/download/onlineMenu-v10/onlineMenu-release-10.zap && \
    rm -rf openapi-* && wget https://github.com/zaproxy/zap-extensions/releases/download/openapi-v30/openapi-beta-30.zap && \
    rm -rf plugnhack-* && wget https://github.com/zaproxy/zap-extensions/releases/download/plugnhack-v13/plugnhack-beta-13.zap && \
    rm -rf portscan-* && wget https://github.com/zaproxy/zap-extensions/releases/download/portscan-v10/portscan-beta-10.zap && \
    rm -rf pscanrules-* && wget https://github.com/zaproxy/zap-extensions/releases/download/pscanrules-v44/pscanrules-release-44.zap && \
    rm -rf pscanrulesBeta-* && wget https://github.com/zaproxy/zap-extensions/releases/download/pscanrulesBeta-v31/pscanrulesBeta-beta-31.zap && \
    rm -rf quickstart-* && wget https://github.com/zaproxy/zap-extensions/releases/download/quickstart-v35/quickstart-release-35.zap && \
    rm -rf replacer-* && wget https://github.com/zaproxy/zap-extensions/releases/download/replacer-v11/replacer-release-11.zap && \
    rm -rf reports-* && wget https://github.com/zaproxy/zap-extensions/releases/download/reports-v0.16.0/reports-release-0.16.0.zap && \
    rm -rf retest-* && wget https://github.com/zaproxy/zap-extensions/releases/download/retest-v0.4.0/retest-alpha-0.4.0.zap && \
    rm -rf retire-* && wget https://github.com/zaproxy/zap-extensions/releases/download/retire-v0.17.0/retire-release-0.17.0.zap && \
    rm -rf reveal-* && wget https://github.com/zaproxy/zap-extensions/releases/download/reveal-v5/reveal-release-5.zap && \
    rm -rf scripts-* && wget https://github.com/zaproxy/zap-extensions/releases/download/scripts-v33/scripts-release-33.zap && \
    rm -rf selenium-* && wget https://github.com/zaproxy/zap-extensions/releases/download/selenium-v15.11.0/selenium-release-15.11.0.zap && \
    rm -rf soap-* && wget https://github.com/zaproxy/zap-extensions/releases/download/soap-v16/soap-beta-16.zap && \
    rm -rf spiderAjax-* && wget https://github.com/zaproxy/zap-extensions/releases/download/spiderAjax-v23.10.0/spiderAjax-release-23.10.0.zap && \
    rm -rf tips-* && wget https://github.com/zaproxy/zap-extensions/releases/download/tips-v10/tips-beta-10.zap && \
    rm -rf webdriverlinux-* && wget https://github.com/zaproxy/zap-extensions/releases/download/webdriverlinux-v46/webdriverlinux-release-46.zap && \
    rm -rf websocket-* && wget https://github.com/zaproxy/zap-extensions/releases/download/websocket-v27/websocket-release-27.zap && \
    rm -rf zest-* && wget https://github.com/zaproxy/zap-extensions/releases/download/zest-v37/zest-beta-37.zap && \
    chown -R zap:zap /zap

# Use custom configuration for ZAP
COPY --chown=zap resources/zap-config.xml /zap/xml/config.xml

# Download scripts and policies
ADD ["$ZAP_POLICIES_URL/API-Minimal.policy",\
     "$ZAP_POLICIES_URL/Default%20Policy.policy",\
     "$ZAP_POLICIES_URL/St-High-Th-High.policy",\
     "$ZAP_POLICIES_URL/St-High-Th-Low.policy",\
     "$ZAP_POLICIES_URL/St-High-Th-Med.policy",\
     "$ZAP_POLICIES_URL/St-Ins-Th-High.policy",\
     "$ZAP_POLICIES_URL/St-Ins-Th-Low.policy",\
     "$ZAP_POLICIES_URL/St-Ins-Th-Med.policy",\
     "$ZAP_POLICIES_URL/St-Low-Th-High.policy",\
     "$ZAP_POLICIES_URL/St-Low-Th-Low.policy",\
     "$ZAP_POLICIES_URL/St-Low-Th-Med.policy",\
     "$ZAP_POLICIES_URL/St-Med-Th-High.policy",\
     "$ZAP_POLICIES_URL/St-Med-Th-Low.policy",\
     "/app/zap/policies/"]

# Install DAST dependencies
COPY requirements.txt /dast-requirements.txt

# Install zapcli/owasp zap seperately from requirements because dependencies are incompatible
COPY --from=compiled_dependencies --chown=root:root /usr/local/bin/mprof /usr/local/bin/mprof
COPY --from=compiled_dependencies --chown=root:staff /usr/local/lib/python3.10/dist-packages /usr/local/lib/python3.10/dist-packages
RUN pip install --no-cache zapcli python-owasp-zap-v2.4 && \
    pip install --no-cache -r /dast-requirements.txt

# Setup the DAST application
COPY profiling /app/profiling
COPY scripts /app/scripts
COPY resources /app/resources
COPY src /app/src
COPY analyze.py README.md CHANGELOG.md LICENSE /app/
COPY analyze /analyze

# Create the work directories, grant user access
# non-zap users should be able to write to work directories (/output, /zap/wrk)
RUN touch /app/building_for.$BUILDING_FOR && \
    mkdir /data && \
    mkdir /output && \
    mkdir -p /app/zap/session && \
    mkdir -p /app/zap/plugin && \
    mkdir -p /app/zap/fuzzers/dirbuster \
             /app/zap/fuzzers/PlugnHack && \
    mkdir -p /app/zap/dirbuster && \
    chown -R zap:zap /app && \
    chown -R zap:zap /data && \
    chown -R zap:zap /output && \
    chmod 777 /app/zap && \
    chmod 777 -R /app/zap/fuzzers && \
    chmod 777 /app/zap/plugin && \
    chmod 777 /app/zap/policies && \
    chmod 777 /app/zap/session && \
    chmod 777 /data && \
    chmod 777 /output && \
    chmod 777 /zap && \
    chmod 666 /app/zap/policies/*.policy && \
    find /app/resources -name '*.js' -exec chmod 644 {} \;

## Run as zap, running as root is not supported
USER zap
WORKDIR /output

ENTRYPOINT []
CMD ["/analyze"]
