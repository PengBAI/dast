from .ajax_spider_server import AjaxSpiderServer
from .basic_site_server import BasicSiteServer
from .django_poll_server import DjangoPollServer
from .dvwa_server import DVWAServer
from .echo_server import EchoServer
from .multi_page_server import MultiPageServer
from .mutual_tls_server import MutualTLSServer
from .pancakes_server import PancakesServer
from .rest_api_server import RestApiServer

server_classes = [AjaxSpiderServer, BasicSiteServer, DjangoPollServer, DVWAServer, EchoServer, MultiPageServer,
                  MutualTLSServer, PancakesServer, RestApiServer]

__all__ = ['AjaxSpiderServer',
           'BasicSiteServer',
           'DVWAServer',
           'EchoServer',
           'MultiPageServer',
           'MutualTLSServer',
           'PancakesServer',
           'RestApiServer',
           'server_classes']
