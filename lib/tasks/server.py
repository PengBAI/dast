from invoke import Collection, run, task

from lib.server import AjaxSpiderServer, BasicSiteServer, DVWAServer, DjangoPollServer, EchoServer, \
    MultiPageServer, MutualTLSServer, PancakesServer, RestApiServer, server_classes


@task
def basicsite(context):
    """Start a BasicSite server."""
    BasicSiteServer(run, port=8010, secure_port=8011).stop().start()


@task
def ajaxspider(context):
    """Start an AjaxSpider server."""
    AjaxSpiderServer(run, port=8020).stop().start()


@task
def restapi(context):
    """Start a RestAPI server."""
    RestApiServer(run, port=8030).stop().start()


@task
def dvwa(context):
    """Start a DVWA server."""
    DVWAServer(run, port=8050).stop().start()


@task
def pancakes(context):
    """Start a Pancakes/Single Page Application server."""
    PancakesServer(run, port=8060).stop().start()


@task
def multipagesite(context):
    """Start a Multi-page Application server."""
    MultiPageServer(run, port=8070).stop().start()


@task
def mutualtls(context):
    """Start a MutualTLS Application server."""
    MutualTLSServer(run, secure_port=8090).stop().start()


@task
def djangosite(context):
    """Start the Django fixture server."""
    DjangoPollServer(run, port=8090).stop().start()


@task
def echo(context):
    """Start an Echo server."""
    EchoServer(run, port=8100).stop().start()


@task
def stopall(context):
    """Stop all servers managed by Invoke."""
    [server_clazz(run).stop() for server_clazz in server_classes]


server = Collection(ajaxspider, basicsite, djangosite,
                    dvwa, echo, multipagesite, mutualtls, pancakes, restapi, stopall)
