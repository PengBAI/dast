import json
import logging
import os
import time
from typing import Any, Dict, List

from zapv2 import ZAPv2 as ZAP

from src.configuration import Configuration
from src.models import Alerts, Target
from src.models.errors import InvalidStateError
from src.models.http import HttpMessages
from src.services import AJAXSpider, ActiveScan, BrowserkerScan, PythonObjectEncoder, \
                         ScanSummaryService, Spider, TargetAvailability, \
                         TargetProbe, TargetSelector, URLScan
from src.services.script_finder import ScriptFinder
from src.utilities import is_url
from src.zap_gateway import Settings
from src.zap_gateway.zap_server_daemon import ZAPServerDaemon
from src.zap_gateway.zaproxy import ZAProxy


class CustomHooks():
    PASSIVE_SCAN_WAIT_SLEEP_SECONDS = 2

    def __init__(self, zaproxy: ZAProxy, report_formatter, system, config: Configuration, script_finder: ScriptFinder):
        self.zaproxy = zaproxy
        self.report_formatter = report_formatter
        self.system = system
        self.config = config
        self._script_finder = script_finder

    def handover_to_dast(self, zap: ZAP, zap_daemon: ZAPServerDaemon) -> None:
        logging.debug('handover_to_dast')

        target = Target(self.config.target)

        self._zap_started(zap, target)

        if self.config.write_addons_to_update_file:
            addons = self.zaproxy.updated_addons()
            with open(f'{Settings.WRK_DIR}addons.json', 'w') as filehandle:
                filehandle.write(json.dumps(addons))

            zap_daemon.shutdown()
            self.system.sys_exit(0)
            return

        target = TargetSelector(self.zaproxy, self.config).select()
        active_scan = ActiveScan(self.zaproxy, target, self.config.is_api_scan)

        if self.config.only_include_rules:
            self.zaproxy.only_include_rules(self.config.only_include_rules, active_scan.policy_name)
        else:
            self.zaproxy.exclude_rules(self.config.exclude_rules, active_scan.policy_name)

        if not self.config.skip_target_check \
           and not self.config.urls_to_scan \
           and self.config.target \
           and is_url(self.config.target):
            TargetAvailability(self.config.target, self.config).verify()

        if self.config.auth_url and not self.config.browserker_scan:
            BrowserkerScan(self.config, target, self.zaproxy).authenticate()

        if not self.config.browserker_scan \
           and not self.config.urls_to_scan \
           and self.config.target \
           and is_url(self.config.target):
            # This second probe is necessary to avoid changing some scan results
            # Browserker scans do not need to preserve this functionality and so do not run this second probe
            # It will be removed when the major version is released
            # https://gitlab.com/gitlab-org/gitlab/-/issues/295205
            TargetProbe(self.config.target, self.config, proxy=zap_daemon.proxy_endpoint()).send_ping()

        logging.info('starting scan')

        browserker_vulnerabilities = []

        if self.config.browserker_scan:
            self.zaproxy.exclude_all_passive_rules()
            browserker_scan_result = BrowserkerScan(self.config, target, self.zaproxy).run()
            browserker_scanned_urls = browserker_scan_result.scanned_urls
            browserker_vulnerabilities = browserker_scan_result.vulnerabilities

            if browserker_scanned_urls:
                URLScan(
                    self.zaproxy,
                    target,
                    browserker_scanned_urls,
                    self.config.exclude_urls,
                ).run()
        elif self.config.urls_to_scan:
            URLScan(
                self.zaproxy,
                target,
                self.config.urls_to_scan,
                self.config.exclude_urls,
            ).run()
        elif not self.config.is_api_scan:
            Spider(self.zaproxy, target).run()

            if self.config.zap_use_ajax_spider:
                AJAXSpider(self.zaproxy, target, self.config.spider_mins).run()

        if self.config.full_scan:
            active_scan.run()

        self._wait_for_passive_scan()

        if self.config.zap_report_html:
            self.zaproxy.write_html_report(self.config.zap_report_html)

        if self.config.zap_report_xml:
            self.zaproxy.write_xml_report(self.config.zap_report_xml)

        if self.config.zap_report_md:
            self.zaproxy.write_md_report(self.config.zap_report_md)

        alerts = self.zaproxy.alerts(target)
        scanned_resources = self.zaproxy.scanned_resources(target)

        self._print_rules(alerts, active_scan.policy_name)

        self._write_dast_report(alerts, scanned_resources, browserker_vulnerabilities)
        self._log_urls(scanned_resources)

        zap_daemon.shutdown()
        self.system.sys_exit(0)

    def _zap_started(self, zap: ZAP, target: Target) -> None:
        logging.debug('zap_started')

        self.zaproxy.new_session(zap)

        # before API scans use a custom context we need to ensure specification/endpoint URLs are added to the context
        if not self.config.is_api_scan:
            self.zaproxy.new_context(target)

        scripts = self._script_finder.find_scripts()

        for script in scripts:
            self.zaproxy.load_script(script)

    def _wait_for_passive_scan(self) -> None:
        remaining_records = self.zaproxy.remaining_records_to_passive_scan()
        time_taken = 0
        timed_out = False
        timeout_in_secs = self.config.passive_scan_max_wait_time * 60

        while remaining_records > 0:
            logging.debug(f'Records to passive scan: {remaining_records}')

            time.sleep(self.PASSIVE_SCAN_WAIT_SLEEP_SECONDS)
            time_taken += self.PASSIVE_SCAN_WAIT_SLEEP_SECONDS

            if timeout_in_secs and time_taken > timeout_in_secs:
                timed_out = True
                break

            remaining_records = self.zaproxy.remaining_records_to_passive_scan()

        if timed_out:
            logging.debug(f'Exceeded passive scan timeout of {timeout_in_secs}s')
        else:
            logging.debug('Passive scanning complete!')

    def _log_urls(self, scanned_resouces: HttpMessages) -> None:
        if len(scanned_resouces.request_summaries()) < 1:
            logging.info('0 URLs were scanned.')
            return

        logging.info(f'The following URLs were scanned:\n'
                     f'{scanned_resouces.human_readable_requests()}')

    def _print_rules(self, alerts: Alerts, active_scan_policy: str) -> None:
        rules = self.zaproxy.executed_rules(active_scan_policy)
        ScanSummaryService(rules, alerts, self.system).print_results()

    def _write_dast_report(self, alerts: Alerts, scanned_resources: HttpMessages,
                           browserker_vulnerabilities: List[Dict[str, Any]]) -> None:
        end_date_time = self.system.current_date_time()
        scan, spider_scan_results = self._spider_scan_results()

        report = self.report_formatter.format_report(
            self.zaproxy.zap_version(),
            alerts,
            scanned_resources,
            end_date_time,
            scan,
            spider_scan_results,
        )

        if self.config.ff_browser_passive_scan_mode:
            for vulnerability in browserker_vulnerabilities:
                report['vulnerabilities'].append(vulnerability)

            report['vulnerabilities'] = self._sort_report_vulnerabilities(report['vulnerabilities'])

        total_vulnerability_count = 0
        if 'vulnerabilities' in report:
            total_vulnerability_count = len(report['vulnerabilities'])

        if total_vulnerability_count > self.config.max_findings:
            logging.warning(f'Detected {total_vulnerability_count} vulnerability findings, \
                           limiting report to only output {"{:,}".format(self.config.max_findings)}')
            report['vulnerabilities'] = report['vulnerabilities'][:self.config.max_findings]

        with open(os.path.join(Settings.WRK_DIR, 'gl-dast-report.json'), 'w') as file:
            json.dump(report, file, cls=PythonObjectEncoder)

    def _sort_report_vulnerabilities(self, vulnerabilities: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
        severity_sort_order = {
            'Critical': 0,
            'High': 1,
            'Medium': 2,
            'Low': 3,
            'Info': 4,
        }

        return sorted(vulnerabilities, key=lambda v: (
            severity_sort_order[v['severity']],
            v['message'],
            v['location']['method'],
            v['location']['path'],
            v['location']['param'],
        ))

    def _spider_scan_results(self):
        if self.config.is_api_scan or self.config.urls_to_scan or self.config.browserker_scan:
            return {'progress': '100', 'state': 'FINISHED', 'id': '0'}, [{'urlsInScope': [{}]}]

        all_scans = self.zaproxy.spider_scans()

        if not all_scans:
            raise InvalidStateError('Error: no scans were performed. ' +
                                    'Check log file zap.out to find out the reason.')

        # baseline scan and full scan should only perform one scan,
        # warn if for some reason multiple scans were performed
        if len(all_scans) > 1:
            logging.warning('More than one scan was executed. ' +
                            'Reporting only results from first run.')

        scan = all_scans[0]
        spider_scan_results = self.zaproxy.scan_results(int(scan['id']))
        return scan, spider_scan_results
