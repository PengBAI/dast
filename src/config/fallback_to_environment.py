from argparse import Action, ArgumentParser, Namespace
from typing import Any, List, MutableMapping, Optional, Sequence, Union


class FallbackToEnvironment(Action):

    def __init__(self, environment_variables: Optional[List[str]], environment: MutableMapping, **kwargs: Any):
        default = kwargs.get('default', None)
        metavar = kwargs.get('metavar', '')

        if environment and environment_variables:
            for var in environment_variables:
                default = environment[var] if var in environment else default

            metavar = environment_variables[0]

        kwargs['default'] = default
        kwargs['metavar'] = metavar
        super(FallbackToEnvironment, self).__init__(**kwargs)

    def __call__(self,
                 parser: ArgumentParser,
                 namespace: Namespace,
                 values: Union[str, Sequence, None],
                 option_string: Optional[str] = None) -> None:
        attr_values = True if self.nargs == 0 else values
        setattr(namespace, self.dest, attr_values)
