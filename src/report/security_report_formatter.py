from collections import OrderedDict
from datetime import datetime
from typing import Any, Dict, List, Sequence

from src.configuration import Configuration
from src.models import Alert, Alerts
from src.models.http import HttpMessages
from .security_report_formatters import ScanReportFormatter, VulnerabilityReportFormatter


class SecurityReportFormatter:

    def __init__(
        self,
        scan_report_formatter: ScanReportFormatter,
        vulnerability_report_formatter: VulnerabilityReportFormatter,
        config: Configuration,
    ):
        self.scan_report_formatter = scan_report_formatter
        self.vulnerability_report_formatter = vulnerability_report_formatter
        self._config = config

    def format_report(
            self,
            zap_version: str,
            alerts: Sequence[Alert],
            scanned_resources: HttpMessages,
            end_date_time: datetime,
            spider_scan: str = '',
            spider_scan_results: str = '',
    ) -> Dict[str, Any]:
        formatted: Dict[str, Any] = OrderedDict()
        formatted['remediations'] = []
        formatted['version'] = '14.1.2'
        formatted['vulnerabilities'] = self._format_vulnerabilities(Alerts(alerts))
        formatted['scan'] = self.scan_report_formatter.format_report(
            scanned_resources, zap_version, end_date_time)

        return formatted

    def _format_vulnerabilities(self, alerts: Alerts) -> List[Dict[str, Any]]:
        alerts = alerts.sort()
        if self._config.aggregate_vulnerabilities or self._config.browserker_scan:
            alerts = alerts.aggregate()

        return [self.vulnerability_report_formatter.format_report(alert) for alert in alerts]
