from subprocess import Popen

from zapv2 import ZAPv2 as ZAP


class ZAPServerDaemon:

    def __init__(self, port: int, process: Popen):
        self._port = port
        self._process = process

    def proxy_endpoint(self) -> str:
        return f'http://{self.ip_address()}:{self.port()}'

    def ip_address(self) -> str:
        return '127.0.0.1'

    def port(self) -> int:
        return self._port

    def shutdown(self) -> None:
        exit_code = self._process.poll()

        if exit_code is not None:
            raise RuntimeError(f'Failed to shutdown ZAP as ZAP has already exited with exit code {exit_code}. '
                               'This indicates an error occurred with the scan')

        address = f'http://{self.ip_address()}:{self.port()}'
        zap_client = ZAP(proxies={'http': address, 'https': address})
        zap_client.core.shutdown()
